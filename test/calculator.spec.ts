import { describe, expect, it } from '@jest/globals';
import { calculate } from '../src/calculator';

describe('calculator', () => {
  describe('calculate()', () => {
    it('should add the two operands', () => {
      const result = calculate('+', 1, 2);

      expect(result).toEqual(3);
    });

    it('should subtract the second operand from the first operand', () => {
      const result = calculate('-', 1, 2);

      expect(result).toEqual(-1);
    })

    it('should multiply the two operands', () => {
      const result = calculate('*', 1, 2);

      expect(result).toEqual(2);
    });

    /*
    it('should divide the first operand with the second operand', () => {
      const result = calculate('/', 1, 2);

      expect(result).toEqual(0.5);
    });
    */
  })
})
