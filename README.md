# Code Coverage Medium Article

The article itself is under `article` folder.

For those who want to try running the test itself:

## Prerequisite

- node (v.18 or above)
- `typescript`
- `ts-node`

## Setup

```
$ git clone
$ cd code-coverage-medium
$ npm i
```

## Run the Test

```
$ npx jest --coverage
```
