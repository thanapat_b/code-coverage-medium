'use strict';

export function round(num: number, decimal: number): number {
  return Number(num.toFixed(decimal));
}

export function calculate(operation: string, op1: number, op2: number): number {
  let result = 0;

  if (operation === '+') {
    result = op1 + op2;
  } else if (operation === '-') {
    result = op1 - op2;
  } else if (operation === '*') {
    result = op1 * op2;
  } else if (operation === '/') {
    result = round(op1 / op2, 2);
  }

  return result;
}

