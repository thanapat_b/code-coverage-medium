# A Brief Introduction to Code Coverage

I believe that those stumble upon this article should be familiar with what unit testing is, or at least have heard the term in passing. Usually when unit testing is brought up in a conversation, the term code coverage is likely to be mentioned. This article will try its best to give you, the readers, a brief introduction (with examples) on code coverage and how you may include it to your coding projects.
Note that this article is written on a specific technology stack (TypeScript with Jest as the test runner), but the content should be applicable to what you are familiar with.

## Brief Backgrounds on Unit Testing

As a refresher, let's revisit what's and why's of unit testing.

Unit testing is the process to create pieces of code that invoke units of work in the system to check their behaviors against sets of assumptions about the behaviors of their respective units. A unit of work may be a function, a class, or a group of code that represents a functionality (e.g. business logic) (Paraphrased from [1](https://www.artofunittesting.com/definition-of-a-unit-test)).

Code snippets below show a very simple implementation of a calculator, and its respective unit test implementation.

```typescript
// calculator.ts
export function round(num: number, decimal: number): number {
  return Number(num.toFixed(decimal));
}

export function calculate(operation: string, op1: number, op2: number): number {
  let result = 0;

  if (operation === '+') {
    result = op1 + op2;
  } else if (operation === '-') {
    result = op1 - op2;
  } else if (operation === '*') {
    result = op1 * op2;
  } else if (operation === '/') {
    result = round(op1 / op2, 2);
  }

  return result;
}

// calculator.spec.ts
describe('calculator', () => {
  describe('calculate()', () => {
    it('should add the two operands', () => {
      const result = calculate('+', 1, 2);

      expect(result).toEqual(3);
    });

    it('should subtract the second operand from the first operand', () => {
      const result = calculate('-', 1, 2);

      expect(result).toEqual(-1);
    })

    it('should multiply the two operands', () => {
      const result = calculate('*', 1, 2);

      expect(result).toEqual(2);
    });

    /* uncomment to achieve all 100%
    it('should divide the first operand with the second operand', () => {
      const result = calculate('/', 1, 2);

      expect(result).toEqual(0.5);
    });
    */
  })
})
```

Some of the common goals of implementing unit tests are:

- To detect potential bugs and/or unintended consequences when the codebase is updated (by implementing new features, editing existing logics, etc.)
- To ensure that our implementation of business logics are as thorough as possible (including success cases, failure cases, error handling, etc.)
- To give developers confidence in the correctness of our implementation that we can control (those not depending on external sources)

So how does code coverage help achieving those goals? Let's discuss about the code coverage first before we explore those benefits.

## What is Code Coverage

Code coverage refers to a group of metrics measuring how well our tests "cover" the codebase. It tells the developers how much of the codebase is visited/executed by the tests we wrote, which in turn indicates how well the tests are written. By this definition, code coverage helps us achieve the first two goals of implementing unit tests described above. (Paraphrased from [2](https://web.dev/articles/ta-code-coverage))

Code coverage usually measures how much of the following components are covered (in percentage): statements, branches, functions, and lines. All metrics will be measured for each file, and then combined according to the codebase folder structure.

Running test with the code coverage option supplied against the above code snippet give the following result:

```sh
$ npx jest --coverage
 PASS  test/calculator.spec.ts
  calculator
    calculate()
      ✓ should add the two operands (1 ms)
      ✓ should subtract the second operand from the first operand
      ✓ should multiply the two operands

---------------|---------|----------|---------|---------|-------------------
File           | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
---------------|---------|----------|---------|---------|-------------------
All files      |   76.92 |    71.42 |      50 |   76.92 |
 calculator.ts |   76.92 |    71.42 |      50 |   76.92 | 4,16-17
---------------|---------|----------|---------|---------|-------------------
Test Suites: 1 passed, 1 total
Tests:       3 passed, 3 total
Snapshots:   0 total
Time:        0.939 s, estimated 1 s
Ran all test suites.
```

Let's take a look at each metrics in detail using the result above as an example.

### Statements VS Lines

At a glance, both metrics seem to represent the same measurement: how much lines of code are covered by the test suite, but one statement can span across multiple lines of code (and vice versa) as shown below:

```typescript
// a statement that fits a line
const someVariable = async someAsyncFunction();

// a statement that spans across multiple lines
const transformedArray = originalArray.map((element) => {
  return element.transform();
});

// multiple statements on a single line, you should not write like this though
const a = 'value1'; const b = 'value2';
```

Variable assignments, function declarations, function calls, loops (for, while), conditions (if-else), are some examples of a statement.
In most cases, these two metrics can be used interchangeably, but keep in mind that there might be some circumstances that one might prefer one metric over the other.

![statements-lines](./images/statements-lines.png)

For our code example, the statement (and line -- all statements occupy a line each) coverage will be 76.92%. All valid statements and lines (excluding lines with only syntax components e.g. close bracket) are count within the file. Then the code coverage reporter will keep track of how many times each line of code is executed, denoted by the green `1x`, `2x` on the left-hand side. These two metrics are then calculated by calculating the percentage of the executed lines and the total number of lines of code. For our code example, as there is no test case for division, the code in the `else if (operation === '/')` block, and the entire `round()` function are not called, highlighted in red in the above image.

### Branches

This metric measures how well our test suite covers different paths within a unit, including conditions (if-else) and loops. You might think of this metric as how well we write our test suite to check if:

- all codes under if-elses within a unit are executed according to their conditions
- all loops are entered
- codes in catch block are executed when we correctly throw
- etc.

![branches](./images/branches.png)

For our code example, since the test case for the division operand is not included in the test suite, the branch coverage is 71.42%. Within the coverage report, `if` and `else` keyword are counted separately, hence 7 as the number of all branches in the code, and 5 as the number of visited branches (three `if`s and two `else`s). All branches that are not visited will be highlighted in red, and are labeled with yellow `I` and/or `E` button (as shown in the above image). `I` label denotes an unvisited `if` statement, and likewise `E` label denotes an unvisited `else` statement.

### Functions

This metric is very straightforward as it measures the number of functions in the codebase that are called by our test suite. Usually we will look at this metric first as it helps us identify if we write unnecessary functions that are not called anywhere in the codebase, or are called in unreachable places (e.g. within an if-else block that always short-circuited to one of the branches).

For our code example, the function coverage will be 50% as only one of two functions are called in the test suite.

## Using the code coverage result

> Note: this is written under the assumption that nyc/istanbul is used as the code coverage report generator

By default, running the test suite with code coverage report will give us two kinds of output:

### Readable report print to the console (see above for an example)

The console report is usually used to plug with CI/CD to obtain the code coverage metrics. The metrics' percentage will be used in order to mark the pipeline as success of failed depending on the configuration of the pipeline. For example, the build pipeline can be stopped if the statement coverage falls under 80%. It can also be plugged into tools like SonarQube for better test suite analysis for example.

### A `coverage` folder containing the report in HTMl format

The more visual HTML format report provides a more detailed look into how the metrics are calculated. Here is the report generated from the above code snippet:

![index.html](./images/index.html.png)

The overview page (`index.html`) will be similar to the console report, but with additional information on the actual numbers of statements, branches, functions, and lines covered by the test suite. Each folder/file will also be marked with colors indicating the health of the metric, ranging from green, yellow/orange, to red.

![calculator.ts](./images/calculator.ts.png)

For each file the test is run on, in addition to the actual number of the metrics above, the number of statement execution, and highlights (in red) of the statements, branches, and functions that are not executed by the test suite are displayed. This is usually where the discussion on how to improve test suite starts.

## Code coverage benefits and caveats

To save you from scrolling to the beginning of the article, here are the goals of implementing unit tests mentioned above:

- To detect potential bugs and/or unintended consequences when the codebase is updated (by implementing new features, editing existing logics, etc.)
- To ensure that our implementation of business logics are as thorough as possible (including success cases, failure cases, error handling, etc.)
- To give developers confidence in the correctness of our implementation that we can control (those not depending on external sources)

Code coverage report contributes heavily to the success of the second goal, as it illustrate where the test suite might be lacking, which in turns reflect how well the test suite is written.

### Caveats

With that said, code coverage metrics do not indicate the quality of the test suite. The following code snippet is an example of the test suite that only aims for 100% of all metrics with zero concerns on actual testing:

```typescript
it('should cover 100% of everything', () => {
  calculate('+', 1, 2);
  calculate('-', 1, 2);
  calculate('*', 1, 2);
  calculate('/', 1, 2);

  expect(true).toBe(true);
})
```

All four operations, and the `round()` function, are called within the test case, but it asserts nothing. This test case is not helpful in anyway.
Because of this caveat, code coverage metrics are not the ironclad indicator of the health of our test suite. We, as a developers, have to review the test suite and make sure that it answers the goals described above.

### Common metric thresholds

In practice, setting an expectation that every metrics must be at 100% is excessive, and will likely incentivize the team to create test cases that only aims to achieve the metric (as shown in the Caveats section). The metrics provided by code coverage report should be used more as a way to track the team's progress on test suite implementation.

Not all codes have to be tested -- constants, simple utility functions with trivial or predictable results, and functions from imported dependencies are some examples of that. From my experience, the proportion of those types of code in a particular code project will be around 15-20%, which I can then imply that we should aim for 80-85% statement and/or line coverage. In a case of introducing unit test to an existing codebase, 70% is a good starting point as that should covers most of the business logic. Branches and functions metrics are usually used to check flaws in the test cases as we may overlook some parts of the business logic.

In short, start with 70%, then aim to hover above 80% overtime.

## Wrapping up

In this article, we discussed a brief definition and goals of unit testing, definitions and examples of code coverage and its metrics, and some of the benefits of code coverage. I hope that you, the readers, have a better idea of what is code coverage and are inspired (a little) to maybe introduce it to your projects.

P.S. This is how the coverage report looks like if the last test case is uncommented:

```sh
$ npx jest --coverage
 PASS  test/calculator.spec.ts
  calculator
    calculate()
      ✓ should add the two operands (1 ms)
      ✓ should subtract the second operand from the first operand (1 ms)
      ✓ should multiply the two operands
      ✓ should divide the first operand with the second operand

---------------|---------|----------|---------|---------|-------------------
File           | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
---------------|---------|----------|---------|---------|-------------------
All files      |     100 |      100 |     100 |     100 |
 calculator.ts |     100 |      100 |     100 |     100 |
---------------|---------|----------|---------|---------|-------------------
Test Suites: 1 passed, 1 total
Tests:       4 passed, 4 total
Snapshots:   0 total
Time:        0.817 s, estimated 1 s
Ran all test suites.```
